
$(function() {

    $("#form").validate({
        rules: {
            name: {
                required: true,
                minlength: 3,
                maxlength: 10
            },
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 5
            },
            passwordConfirm: {
                required: true,
                equalTo: "#inputPassword"
            }
        },
        errorPlacement: function(error, element) {
            $(element)
                .parent()
                .addClass("has-error");
            error.addClass("help-block").appendTo(element.parent("div"));
        },
        unhighlight: function(element) {
            $(element)
                .parent()
                .addClass("has-success")
                .removeClass("has-error");
        }

    });
});